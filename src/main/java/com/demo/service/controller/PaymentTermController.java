package com.demo.service.controller;

import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.service.dto.PaymentRequestDto;
import com.demo.service.dto.PaymentResponseDto;
import com.demo.service.entity.PaymentTermsEntity;
import com.demo.service.services.PaymentService;

@RestController
@RequestMapping("/paymentTerms")
public class PaymentTermController {
	
	private PaymentService service;
	
	public PaymentTermController(PaymentService service)
	{
		this.service = service;
	}
	
	@GetMapping("/fetchAll")
	public ResponseEntity<List<PaymentTermsEntity>> getPaymentTerms()
	{
		List<PaymentTermsEntity> paymentTerms = service.getPaymentTerms();
		
		return new ResponseEntity<>(paymentTerms, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<PaymentResponseDto> createPaymentTerm(@RequestBody PaymentRequestDto request ){
		
        ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		PaymentTermsEntity paymentEntity = mapper.map(request, PaymentTermsEntity.class);
		PaymentResponseDto response = service.createPaymentTerm(paymentEntity);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<PaymentResponseDto> updatePaymentTerm(@PathVariable("id") Integer id,@RequestBody PaymentRequestDto request ){
		
		PaymentTermsEntity entity = service.getPaymentTermById(id).get();
		entity.setCode(request.getCode());
		entity.setDescription(request.getDescription());
		entity.setDays(request.getDays());
		entity.setRemindBeforeDays(request.getRemindBeforeDays());
		PaymentResponseDto response = service.createPaymentTerm(entity);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity deletePaymentTerm(@PathVariable("id") Integer id) {
		service.deletePaymentTerm(id);
		return new ResponseEntity(HttpStatus.NO_CONTENT); 
	}
	
	@GetMapping("/fetchByTermName/{id}")
	public ResponseEntity<PaymentTermsEntity> getPaymentTermById(Integer id)
	{
		PaymentTermsEntity entity = service.getPaymentTermById(id).get();
		
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

}