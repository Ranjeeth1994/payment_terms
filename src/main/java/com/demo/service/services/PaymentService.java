package com.demo.service.services;

import java.util.List;
import java.util.Optional;

import com.demo.service.dto.PaymentResponseDto;
import com.demo.service.entity.PaymentTermsEntity;

public interface PaymentService {
	
	public PaymentResponseDto createPaymentTerm(PaymentTermsEntity entity);
	
	public void deletePaymentTerm(Integer id);
	
	public List<PaymentTermsEntity> getPaymentTerms();
	
	public Optional<PaymentTermsEntity> getPaymentTermById(Integer id);

}
