package com.demo.service.services;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.demo.service.dto.PaymentResponseDto;
import com.demo.service.entity.PaymentTermsEntity;
import com.demo.service.repository.PaymentTermsRepository;

@Service
public class PaymentTermServiceImplementation implements PaymentService {
	
	
     private PaymentTermsRepository repository;
     
     public PaymentTermServiceImplementation(PaymentTermsRepository repository)
     {
    	 this.repository = repository;
     }

	@Override
	public PaymentResponseDto createPaymentTerm(PaymentTermsEntity paymentEntity) {
		// TODO Auto-generated method stub
		ModelMapper mapper = new ModelMapper();
		PaymentTermsEntity entityResponse = repository.save(paymentEntity);
		PaymentResponseDto responseDto = mapper.map(entityResponse, PaymentResponseDto.class);
		return responseDto;
	}


	@Override
	public void deletePaymentTerm(Integer id) {
		// TODO Auto-generated method stub
		repository.deleteById(id);
	}

	@Override
	public List<PaymentTermsEntity> getPaymentTerms() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Optional<PaymentTermsEntity> getPaymentTermById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}
     
}