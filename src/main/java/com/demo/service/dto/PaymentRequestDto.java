package com.demo.service.dto;

public class PaymentRequestDto {
	
	private String code;
	private String description;
	private Integer days;
	private Integer remindBeforeDays;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
	public Integer getRemindBeforeDays() {
		return remindBeforeDays;
	}
	public void setRemindBeforeDays(Integer remindBeforeDays) {
		this.remindBeforeDays = remindBeforeDays;
	}

}
