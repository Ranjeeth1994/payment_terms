package com.demo.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentTermServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentTermServiceApplication.class, args);
	}

}
